<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\Grocery;
use App\RawMaterial;
use App\Unit_measurement;
use App\movement_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;  

class InventoryController extends Controller
{
    public $message;
    public $code;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inventories = DB::select("SELECT a.raw_material_id, a.grocery_id, a.movement_type_id,a.id, b.description materiaPrima, c.description bodega, d.description mov, e.unit, a.created_at, a.amount, a.document, a.document_value, a.price, a.observation
        FROM inventories a
        LEFT JOIN raw_materials b ON a.raw_material_id = b.id
        LEFT JOIN groceries c ON a.grocery_id = c.id
        LEFT JOIN movement_types d ON a.movement_type_id = d.id
        LEFT JOIN unit_measurements e ON b.unit_id = e.id");
        $materias = RawMaterial::with('unit_measurement')->orderBy('description', 'ASC')->get();
        $bodegas = Grocery::orderBy('description', 'ASC')->get();
        $types = movement_type::orderBy('description', 'ASC')->get();
        return response()->json(['movimientos'=>$inventories, 'materias'=> $materias,'bodegas'=>$bodegas,'types'=>$types,'code'=> $this->code,'message' => $this->message  ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->movement_type_id == 2){
            $stockInventario = DB::select("SELECT SUM( a.amount ) As totalEntrada, SUM(c.totalSalida) As totalSalida
                            FROM inventories a LEFT JOIN (SELECT SUM( b.amount ) AS totalSalida, b.raw_material_id, b.grocery_id 
                            FROM inventories b
                            WHERE b.movement_type_id =2
                            GROUP BY b.raw_material_id, b.grocery_id ) c ON a.raw_material_id = c.raw_material_id AND a.grocery_id = c.grocery_id
                            WHERE a.movement_type_id =1 AND a.raw_material_id =".$request->raw_material_id." AND a.grocery_id =".$request->grocery_id."
                            GROUP BY a.raw_material_id, a.grocery_id");
            if(count($stockInventario)){
                $totalStock = $stockInventario[0]->totalEntrada - ($stockInventario[0]->totalSalida + $request->amount);
                if($totalStock < 0){
                    $this->code ='406';
                    $this->message ='Error: Inventario no disponible';
                    return $this->index(); 
                }
            }else{
                $this->code ='406';
                $this->message ='Error: Inventario no disponible';
                return $this->index(); 
            }
            
        }
        $materia_prima = RawMaterial::where('id', $request->raw_material_id)->get();

        $cantidadMov = $request->amount;
        $cantidadMat = $materia_prima[0]->amount;
        $precioMat = $materia_prima[0]->price;

        $price = ($cantidadMov / $cantidadMat) * $precioMat;
        $request->merge([ 'price' => $price]);
        /*$users = DB::table('modules')
                     ->select(DB::raw('count(*) as user_count'))
                     ->where('path', '<>', '')
                     ->groupBy('icon')
                     ->get();*/
        $inventory = Inventory::create($request->all());
        return $this->index(); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(inventory $inventory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(inventory $inventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, inventory $inventory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Inventory::where('id', $id)->delete();
        return $this->index();
    }
}
