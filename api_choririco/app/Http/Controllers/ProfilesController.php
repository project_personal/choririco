<?php

namespace App\Http\Controllers;

use App\Profiles;
use Illuminate\Http\Request;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Profiles::with('modules')->get();
        return $profiles;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profile = Profiles::create($request->all());
        $profile->modules()->attach($request->modules);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function show($profile)
    {
        $profiles = Profiles::where('id',$profile)->with(array('modules'=>function($query){
            $query->select()->orderBy('module_id', 'ASC')->get();;
        }))->get();
        return $profiles;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function edit(Profiles $profiles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $profile)
    {
        $profiles = Profiles::find($profile);
        $profiles->description = $request->description;
        $profiles->observation = $request->observation;
        $profiles->save();
        $profiles->modules()->detach($profiles->modules);
        $profiles->modules()->attach($request->modules);
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function destroy($profile)
    {
        Profiles::where('id', $profile)->delete();
        return $this->index();
    }
}
