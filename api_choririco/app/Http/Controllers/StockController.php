<?php

namespace App\Http\Controllers;

use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;  

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stockInventario = DB::select("SELECT d.id ,d.description, e.unit, f.description AS bodega,SUM( a.amount ) As totalEntrada, SUM(c.totalSalida) AS totalSalida, d.price 
        FROM inventories a LEFT JOIN (SELECT SUM( b.amount ) AS totalSalida, b.raw_material_id, b.grocery_id FROM inventories b WHERE b.movement_type_id =2 GROUP BY b.raw_material_id, b.grocery_id ) c ON a.raw_material_id = c.raw_material_id AND a.grocery_id = c.grocery_id
        INNER JOIN raw_materials d ON a.raw_material_id = d.id
        INNER JOIN unit_measurements e ON d.unit_id = e.id
        INNER JOIN groceries f ON a.grocery_id = f.id
        WHERE a.movement_type_id =1
        GROUP BY a.raw_material_id, a.grocery_id, d.id ,d.description, e.unit, f.description, d.price ");
        return response()->json(['stockInventario'=>$stockInventario ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }
}
