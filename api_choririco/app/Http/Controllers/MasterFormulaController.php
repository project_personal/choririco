<?php

namespace App\Http\Controllers;

use App\master_formula;
use Illuminate\Http\Request;
use App\Unit_measurement;
use App\RawMaterial;
use App\formula_detail;
use App\Product;


class MasterFormulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $master_formula = master_formula::with(['formula_detail.raw_material','formula_detail.unit_measurement'])->with('unit_measurement')->with('products')->get();
        $unidades = Unit_measurement::where('type',1)->orderBy('unit', 'ASC')->get();
        $unidades2 = Unit_measurement::where('type',2)->orderBy('unit', 'ASC')->get();
        $products = Product::orderBy('description', 'ASC')->get();
        return response()->json(['formula_maestra'=>$master_formula,'unidades'=>$unidades,'unidades2'=>$unidades2,'products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formula = master_formula::create($request->all());
        foreach($request->detail as $dato){
            $detail = new formula_detail;
            $detail->raw_material_id = $dato['material']['id'];
            $detail->unit_id = $dato['unit_id'];
            $detail->quantity =$dato['quantity'];
            $detail->ordering = 0;
            $formula->formula_detail()->save($detail); 
        }
        return $this->index();  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\master_formula  $master_formula
     * @return \Illuminate\Http\Response
     */
    public function show(master_formula $master_formula)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\master_formula  $master_formula
     * @return \Illuminate\Http\Response
     */
    public function edit(master_formula $master_formula)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\master_formula  $master_formula
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        formula_detail::where('master_formula_id', $id)->delete();
        $formula = master_formula::find($id);
        $formula->description = $request->description;
        $formula->observation = $request->observation;
        $formula->unit_id = $request->unit_id;
        $formula->quantity = $request->quantity;
        $formula->product_id = $request->product_id;
        $formula->save();

        foreach($request->detail as $dato){
            $detail = new formula_detail;
            $detail->raw_material_id = $dato['material']['id'];
            $detail->unit_id = $dato['unit_id'];
            $detail->quantity =$dato['quantity'];
            $detail->ordering = 0;
            $formula->formula_detail()->save($detail); 
        }
        return $this->index();  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\master_formula  $master_formula
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        master_formula::where('id', $id)->delete();
        return $this->index();  
    }
}
