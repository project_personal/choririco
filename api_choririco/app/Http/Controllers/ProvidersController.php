<?php

namespace App\Http\Controllers;

use App\Providers;
use App\Department;
use App\City;
use Illuminate\Http\Request;

class ProvidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Providers::with('city')->get();
        $departments = Department::orderBy('description', 'ASC')->get();
        $cities = City::orderBy('description', 'ASC')->get();
        return response()->json(['providers'=>$providers, 'departments'=> $departments,'cities'=>$cities ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provider = Providers::create($request->all());
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Providers  $providers
     * @return \Illuminate\Http\Response
     */
    public function show(Providers $providers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Providers  $providers
     * @return \Illuminate\Http\Response
     */
    public function edit(Providers $providers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Providers  $providers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $provider)
    {
        $provider = Providers::find($provider);
        $provider->nit = $request->nit;
        $provider->compamy_name = $request->compamy_name;
        $provider->phone = $request->phone;
        $provider->movil = $request->movil;
        $provider->address = $request->address;
        $provider->email = $request->email;
        $provider->city_id = $request->city_id;
        $provider->department_id = $request->department_id;
        $provider->save();

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Providers  $providers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Providers::where('id', $id)->delete();
        return $this->index();
    }
}
