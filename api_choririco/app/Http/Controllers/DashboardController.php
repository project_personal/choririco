<?php

namespace App\Http\Controllers;

use App\Dashboard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;  

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informacion = DB::select("SELECT COUNT(a.id) informe FROM inventories a WHERE a.movement_type_id =1
                                    UNION ALL
                                    SELECT COUNT(a.id) FROM inventories a WHERE a.movement_type_id =2
                                    UNION ALL
                                    SELECT COUNT(a.id) FROM order_requests a
                                    UNION ALL
                                    SELECT COUNT(a.id) FROM customers a");
        $zones = DB::select("SELECT b.id, b.name, COUNT(a.id) cantidad
                                FROM order_requests a
                                RIGHT JOIN zones b ON a.zone_id = b.id
                                GROUP BY a.zone_id, b.id, b.name");

        $orders = DB::select("SELECT a.status proceso, COUNT(a.id) cantidad
                            FROM production_orders a
                            GROUP BY a.status");

        return response()->json(['informacion'=>$informacion, 'zones'=>$zones, 'orders'=>$orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function show(Dashboard $dashboard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function edit(Dashboard $dashboard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dashboard $dashboard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dashboard $dashboard)
    {
        //
    }
}
