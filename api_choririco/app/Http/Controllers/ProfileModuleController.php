<?php

namespace App\Http\Controllers;

use App\profile_module;
use Illuminate\Http\Request;

class ProfileModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\profile_module  $profile_module
     * @return \Illuminate\Http\Response
     */
    public function show(profile_module $profile_module)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\profile_module  $profile_module
     * @return \Illuminate\Http\Response
     */
    public function edit(profile_module $profile_module)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\profile_module  $profile_module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, profile_module $profile_module)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\profile_module  $profile_module
     * @return \Illuminate\Http\Response
     */
    public function destroy(profile_module $profile_module)
    {
        //
    }
}
