<?php

namespace App\Http\Controllers;

use App\Unit_measurement;
use Illuminate\Http\Request;

class UnitMeasurementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unit_measurement  $unit_measurement
     * @return \Illuminate\Http\Response
     */
    public function show(Unit_measurement $unit_measurement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit_measurement  $unit_measurement
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit_measurement $unit_measurement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit_measurement  $unit_measurement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit_measurement $unit_measurement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit_measurement  $unit_measurement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit_measurement $unit_measurement)
    {
        //
    }
}
