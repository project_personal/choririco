<?php

namespace App\Http\Controllers;

use App\Production_order;
use App\Inventory;
use Illuminate\Http\Request;
use App\master_formula;
use App\Unit_measurement;
use App\Order_detail;
use App\Product;
use Illuminate\Support\Facades\DB;  

class ProductionOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $master_formula = master_formula::all();
        $orders = Production_order::with(['order_detail.unit_measurement','order_detail.raw_material'])->with('master_formula')->get();
        return response()->json(['formula_maestra'=>$master_formula,'orders'=> $orders]);
    }

    public function getProductionOrder(Request $request){
        $orders = Production_order::with(['order_detail.unit_measurement','order_detail.raw_material'])->with(['master_formula.products','master_formula.unit_measurement'])->where('status',$request->status)->get();
        return $orders;
    }

    public function setStatusProductionOrder(Request $request){

        $order = Production_order::find($request->id);
        $order->status = $request->newStatus;
        $order->save();
        
        switch ($request->newStatus) {
            case 'Cancelado':
                DB::table('inventories')->where('document', '=', 'OP-'.str_pad($request->id, 5, "0", STR_PAD_LEFT))->delete();
                break;
            case 'Finalizado':
                $product = Product::find($request->master_formula['product_id']); 
                $product->quantity_available = $product->quantity_available + ($request->amount * ($request->master_formula['quantity']*$request->master_formula['unit_measurement']['equivalence']));
                $product->save(); 
                break;
        }

        return $this->getProductionOrder($request);
    }

    public function getFormulaDetail(Request $request){
        $details = DB::select("SELECT * FROM cantidad_req_formula WHERE IdFormula = ".$request->master_formula_id);
        return $details;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([ 'status' => 'Pendiente']);
        $order = Production_order::create($request->all());

        foreach ($request->detail as $valor){
            $detail = new Order_detail;
            $detail->raw_material_id = $valor['IdMateria'];
            $detail->unit_id = $valor['unitMateria'];
            $detail->required_quantity = $valor['totalRequerida'];
            $detail->quantity_available = $valor['totalDisponible'];
            $order->order_detail()->save($detail); 
            
            $stockInventario = DB::select("SELECT a.grocery_id ,SUM( a.amount ) As totalEntrada, SUM(c.totalSalida) As totalSalida
            FROM inventories a LEFT JOIN (SELECT SUM( b.amount ) AS totalSalida, b.raw_material_id, b.grocery_id 
            FROM inventories b
            WHERE b.movement_type_id =2
            GROUP BY b.raw_material_id, b.grocery_id ) c ON a.raw_material_id = c.raw_material_id AND a.grocery_id = c.grocery_id
            WHERE a.movement_type_id =1 AND a.raw_material_id = ".$valor['IdMateria']." 
            GROUP BY a.raw_material_id, a.grocery_id");
            
            $cantRequerida = $valor['totalRequerida'];
            $cantRequerida2 = $valor['totalRequerida'];
            $arrayBodega = [];  
            $arrayBodegaAmount = [];  
            foreach ($stockInventario as $valor2){
                $cantVal = $valor2->totalEntrada - $valor2->totalSalida;
                if($cantVal > 0 && $cantRequerida > 0){
                    $cantRequerida = $cantRequerida -$cantVal;
                    $arrayBodega[] = $valor2->grocery_id;
                    if($cantRequerida <= 0){
                        $arrayBodegaAmount[] = $cantRequerida2;
                    }else{
                        $arrayBodegaAmount[] = $cantRequerida2-$cantRequerida;
                        $cantRequerida2= $cantRequerida2 - ($cantRequerida2-$cantRequerida);
                    }
                }
            }
            
            foreach($arrayBodega as $key => $bodega){
                $cantAmount = $arrayBodegaAmount[$key];
                $inventory = new Inventory;
                $inventory->raw_material_id = $valor['IdMateria']; 
                $inventory->grocery_id = $bodega; 
                $inventory->movement_type_id = 2; 
                $inventory->amount = $cantAmount; 
                $inventory->price = $valor['price']; 
                $inventory->document = 'OP-'.str_pad($order->id, 5, "0", STR_PAD_LEFT); 
                $inventory->document_value = $cantAmount*$valor['price']; 
                $inventory->observation = 'Salida Inventario por (OP-'.str_pad($order->id, 5, "0", STR_PAD_LEFT).')'; 
                $inventory->save();
            }
            

            
        }
        return $this->index();  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Production_order  $production_order
     * @return \Illuminate\Http\Response
     */
    public function show(Production_order $production_order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Production_order  $production_order
     * @return \Illuminate\Http\Response
     */
    public function edit(Production_order $production_order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Production_order  $production_order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Production_order $production_order)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Production_order  $production_order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Production_order $production_order)
    {
        //
    }
}
