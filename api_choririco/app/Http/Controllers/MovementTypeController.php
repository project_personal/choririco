<?php

namespace App\Http\Controllers;

use App\movement_type;
use Illuminate\Http\Request;

class MovementTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\movement_type  $movement_type
     * @return \Illuminate\Http\Response
     */
    public function show(movement_type $movement_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\movement_type  $movement_type
     * @return \Illuminate\Http\Response
     */
    public function edit(movement_type $movement_type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\movement_type  $movement_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, movement_type $movement_type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\movement_type  $movement_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(movement_type $movement_type)
    {
        //
    }
}
