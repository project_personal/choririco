<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Document_type;
use App\Zone;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::with('document_type')->with('zones')->get();
        $documents = Document_type::orderBy('description', 'ASC')->get();
        $zonas = Zone::orderBy('name', 'ASC')->get();
        return response()->json(['employees'=>$employees,'zonas'=>$zonas ,'documents' => $documents ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = Employee::create($request->all());
        $employee->zones()->attach($request->zonas);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $employee)
    {
        $employee = Employee::find($employee);
        $employee->document_type_id = $request->document_type_id;
        $employee->document_number = $request->document_number;
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->mobile = $request->mobile;
        $employee->address = $request->address;
        $employee->save();
        $employee->zones()->detach($employee->zonas);
        $employee->zones()->attach($request->zonas);
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($employee)
    {
        Employee::where('id', $employee)->delete();
        return $this->index();
    }
}
