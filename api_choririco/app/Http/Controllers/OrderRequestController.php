<?php

namespace App\Http\Controllers;

use App\Order_request;
use Illuminate\Http\Request;
use App\Zone;
use App\Employee;
use App\Customer;
use App\Product;
use App\Request_detail;

class OrderRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = Order_request::with('Zone')->with('customer')->with('employee')->with('request_detail.product')->get();
        $zone = zone::orderBy('name', 'ASC')->get();
        $employee = Employee::orderBy('name', 'ASC')->with('zones')->get();
        $products = Product::orderBy('description', 'ASC')->where('quantity_available','>',0)->get(); 
        return response()->json(['requests'=>$requests,'zone'=>$zone,'employee'=>$employee,'products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([ 'customer_id' => $request->customer_id["id"]]);
        $requests = Order_request::create($request->all());
        $arrayProducts = [];
        foreach($request->detail as $dato){
            $detail = new Request_detail;
            $detail->product_id = $dato['codigo'];
            $detail->quantity = $dato['cantRequerida'];
            $requests->request_detail()->save($detail); 
            $arrayProducts[$dato['codigo']] = $dato['cantRequerida'];
        }
        foreach ($arrayProducts as $key => $value) {
            $products = Product::find($key);
            $products->quantity_available -=  $value;
            $products->save();
        }
        return $this->index();  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order_request  $order_request
     * @return \Illuminate\Http\Response
     */
    public function show(Order_request $order_request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order_request  $order_request
     * @return \Illuminate\Http\Response
     */
    public function edit(Order_request $order_request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order_request  $order_request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order_request $order_request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order_request  $order_request
     * @return \Illuminate\Http\Response
     */
    public function destroy($order_request)
    {
        $order_requests = Order_request::find($order_request)->with('request_detail')->get();
        $total =0;

        foreach ($order_requests[0]->request_detail as $key => $value) {
            $products = Product::find($value['product_id']);
            $products->quantity_available =$products->quantity_available + $value['quantity'];
            $products->save();
        }
        Order_request::where('id', $order_request)->delete();
        return $this->index();
    }
}
