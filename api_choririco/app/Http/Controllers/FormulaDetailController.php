<?php

namespace App\Http\Controllers;

use App\formula_detail;
use Illuminate\Http\Request;

class FormulaDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\formula_detail  $formula_detail
     * @return \Illuminate\Http\Response
     */
    public function show(formula_detail $formula_detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\formula_detail  $formula_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(formula_detail $formula_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\formula_detail  $formula_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, formula_detail $formula_detail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\formula_detail  $formula_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(formula_detail $formula_detail)
    {
        //
    }
}
