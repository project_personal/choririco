<?php

namespace App\Http\Controllers;

use App\RawMaterial;
use App\Providers;
use Illuminate\Http\Request;
use App\Unit_measurement;

class RawMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Providers::orderBy('compamy_name', 'ASC')->get();
        $RawMaterial = RawMaterial::with('provider')->with('unit_measurement')->get();
        $unidades = Unit_measurement::where('type',1)->orderBy('unit', 'ASC')->get();
        return response()->json(['providers'=>$providers, 'rawMaterial'=> $RawMaterial,'unidades'=>$unidades]);
        //return $RawMaterial;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $RawMaterial = RawMaterial::create($request->all());
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RawMaterial  $rawMaterial
     * @return \Illuminate\Http\Response
     */
    public function show(RawMaterial $rawMaterial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RawMaterial  $rawMaterial
     * @return \Illuminate\Http\Response
     */
    public function edit(RawMaterial $rawMaterial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RawMaterial  $rawMaterial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$rawMaterial)
    {
        $rawMaterial = RawMaterial::find($rawMaterial);
        $rawMaterial->description = $request->description;
        $rawMaterial->observation = $request->observation;
        $rawMaterial->provider_id = $request->provider_id;
        $rawMaterial->unit_id = $request->unit_id;
        $rawMaterial->amount = $request->amount;
        $rawMaterial->price = $request->price;
        $rawMaterial->save();

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RawMaterial  $rawMaterial
     * @return \Illuminate\Http\Response
     */
    public function destroy($rawMaterial)
    {
        RawMaterial::where('id', $rawMaterial)->delete();
        return $this->index();
    }
}
