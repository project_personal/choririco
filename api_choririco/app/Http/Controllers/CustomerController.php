<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Document_type;
use App\Department;
use App\Zone;
use App\City;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response 
     */
    public function index()
    {
        $customers = Customer::with('city')->with('department')->with('document_type')->with('zone')->get();
        $departments = Department::orderBy('description', 'ASC')->get();
        $cities = City::orderBy('description', 'ASC')->get();
        $documents = Document_type::orderBy('description', 'ASC')->get();
        $zonas = Zone::orderBy('name', 'ASC')->get();
        return response()->json(['customers'=>$customers,'zonas'=>$zonas ,'departments'=> $departments,'cities'=>$cities,'documents' => $documents ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = Customer::create($request->all());
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $customer)
    {
        $customer = Customer::find($customer);
        $customer->document_type_id = $request->document_type_id;
        $customer->document_number = $request->document_number;
        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->mobile = $request->mobile;
        $customer->address = $request->address;
        $customer->email = $request->email;
        $customer->city_id = $request->city_id;
        $customer->zone_id = $request->zone_id;
        $customer->department_id = $request->department_id;
        $customer->save();

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::where('id', $id)->delete();
        return $this->index();
    }
}
