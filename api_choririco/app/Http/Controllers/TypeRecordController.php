<?php

namespace App\Http\Controllers;

use App\TypeRecord;
use Illuminate\Http\Request;

class TypeRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = TypeRecord::all();
        return $types;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypeRecord  $typeRecord
     * @return \Illuminate\Http\Response
     */
    public function show(TypeRecord $typeRecord)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TypeRecord  $typeRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeRecord $typeRecord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TypeRecord  $typeRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $typeRecord)
    {
        $provider = TypeRecord::find($typeRecord);
        $provider->description = $request->description;
        $provider->save();
        return response()->json(['message'=>'Registro Eliminado']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TypeRecord  $typeRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeRecord $typeRecord)
    {
        //
    }
}
