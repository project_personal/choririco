<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ];

    }

    public function messages()
    {
        return [
            'name.required' => 'Campo nombre requerido.',
            'email.required' =>'Campo Email requerido.',
            'email.unique' => 'Este email ya existe en la base de datos.',
            'password.confirmed' => 'La confirmación de la contraseña no coincide.',
        ];
    }
}