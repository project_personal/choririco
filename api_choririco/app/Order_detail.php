<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    protected $fillable = ["id","production_order_id","raw_material_id","unit_id","required_quantity","quantity_available","created_at"]; 

    public function production_order()
    {
        return $this->belongsTo(Production_order::class, 'production_order_id', 'id');
    }

    public function unit_measurement()
    {
        return $this->belongsTo(Unit_measurement::class, 'unit_id', 'id');
    }

    public function raw_material()
    {
        return $this->belongsTo(RawMaterial::class, 'raw_material_id', 'id');
    }
}
