<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ["id","document_type_id","document_number","name","email","mobile","address","created_at"];

    public function document_type()
    {
        return $this->belongsTo(Document_type::class, 'document_type_id', 'id');
    }

    public function zones() {
        return $this->belongsToMany('App\Zone');
    }
}
