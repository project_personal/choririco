<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ["id","description","department_id","created_at","updated_at"];

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }
}
