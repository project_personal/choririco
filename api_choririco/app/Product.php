<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ["description","observation","typeProduct_id","quantity_available"]; 
 
    public function typeProduct()
    {
        return $this->belongsTo(TypeProduct::class, 'typeProduct_id', 'id');
    }
}
