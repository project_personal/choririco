<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class formula_detail extends Model
{
    protected $fillable = ["id","master_formula_id","raw_material_id","unit_id","ordering","quantity"]; 

    public function unit_measurement()
    {
        return $this->belongsTo(Unit_measurement::class, 'unit_id', 'id');
    }

    public function master_formula()
    {
        return $this->belongsTo(master_formula::class, 'master_formula_id', 'id');
    }

    public function raw_material()
    {
        return $this->belongsTo(RawMaterial::class, 'raw_material_id', 'id');
    }
}
