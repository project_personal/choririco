<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
    protected $fillable = ["nit","compamy_name","department_id","phone","movil","address","email","city_id"]; 

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }
}
