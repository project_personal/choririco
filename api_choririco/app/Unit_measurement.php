<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit_measurement extends Model
{
    protected $fillable = ["id","unit","type","symbol","equivalence"];
}
