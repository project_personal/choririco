<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ["id","document_type_id","document_number","name","email","phone","address","city_id","department_id","mobile","zone_id","created_at"]; 

    public function document_type()
    {
        return $this->belongsTo(Document_type::class, 'document_type_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id', 'id');
    }
}
