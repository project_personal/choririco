<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class movement_type extends Model
{
    protected $fillable = ["id","description"];
}
