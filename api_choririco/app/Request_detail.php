<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request_detail extends Model
{
    protected $fillable = ["id","order_request_id","product_id","quantity","created_at"];

    public function order_request()
    {
        return $this->belongsTo(Order_request::class, 'order_request_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
