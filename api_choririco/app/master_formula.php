<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class master_formula extends Model
{
    protected $fillable = ["id","description","product_id","observation","quantity","unit_id"]; 

    public function unit_measurement() 
    {
        return $this->belongsTo(Unit_measurement::class, 'unit_id', 'id');
    }

    public function formula_detail()
    {
        return $this->hasMany(formula_detail::class);
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
