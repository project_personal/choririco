<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $fillable = ["id","name","equivalent","created_at"]; 

    public function employees() {
        return $this->belongsToMany('App\Employee');
    }
}
