<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_request extends Model
{
    protected $fillable = ["id","customer_id","address","zone_id","employee_id","delivery_date","observation","created_at"];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function request_detail()
    {
        return $this->hasMany(Request_detail::class);
    }
}
