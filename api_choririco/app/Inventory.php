<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = ["raw_material_id","grocery_id","movement_type_id","id","amount","price","observation","document","document_value"]; 


    public function raw_material()
    {
        return $this->belongsTo(RawMaterial::class, 'raw_material_id', 'id');
    }

    public function grocery()
    {
        return $this->belongsTo(Grocery::class, 'grocery_id', 'id');
    }

    public function movement_type() 
    {
        return $this->belongsTo(movement_type::class, 'movement_type_id', 'id');
    }
}
