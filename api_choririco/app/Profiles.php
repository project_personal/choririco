<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
    protected $fillable = ["id","description","observation","created_at","updated_at"];

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function modules() {
        return $this->belongsToMany('App\Module');
    }
}
