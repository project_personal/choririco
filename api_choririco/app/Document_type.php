<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document_type extends Model
{
    protected $fillable = ["id","description","symbol","created_at"]; 
}
