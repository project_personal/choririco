<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Production_order extends Model
{
    protected $fillable = ["id","amount","status","master_formula_id","created_at"];  

    public function master_formula()
    {
        return $this->belongsTo(master_formula::class, 'master_formula_id', 'id');
    }

    public function order_detail()
    {
        return $this->hasMany(Order_detail::class);
    }
}
