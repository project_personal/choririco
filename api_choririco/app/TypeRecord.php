<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeRecord extends Model
{
    protected $fillable = ["description","observation"];
}