<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = ["id","description","observation","icon","path","created_at","updated_at"];

    public function profiles() {
        return $this->belongsToMany('App\Profiles');
    }
}
