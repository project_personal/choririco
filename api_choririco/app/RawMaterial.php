<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RawMaterial extends Model
{
    protected $fillable = ["description","observation","provider_id","unit_id","amount","price"]; 

    public function provider()
    {
        return $this->belongsTo(Providers::class, 'provider_id', 'id');
    }

    public function unit_measurement()
    {
        return $this->belongsTo(Unit_measurement::class, 'unit_id', 'id');
    }

}
