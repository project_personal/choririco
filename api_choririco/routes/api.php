<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('sendPasswordResetLink', 'ResetPasswordController@sendEmail');
    Route::post('resetPassword', 'ChangePasswordController@process');
    Route::apiResource("profiles","ProfilesController");
    Route::apiResource("users","UserController");
    Route::apiResource("modules","ModuleController");
    Route::apiResource("providers","ProvidersController");
    Route::apiResource("rawMaterial","RawMaterialController");
    Route::apiResource("products","ProductController");
    Route::apiResource("groceries","GroceryController");
    Route::apiResource("inventories","InventoryController");
    Route::apiResource("stock","StockController");
    Route::apiResource("formulamaestra","MasterFormulaController");
    Route::apiResource("productionOrder","ProductionOrderController");
    Route::apiResource("customers","CustomerController");
    Route::apiResource("zonas","ZoneController");
    Route::apiResource("employees","EmployeeController");
    Route::apiResource("request","OrderRequestController");
    Route::apiResource("dashboard","DashboardController");
    Route::post('detailFormula', 'ProductionOrderController@getFormulaDetail');
    Route::post('getProductionOrder', 'ProductionOrderController@getProductionOrder');
    Route::post('setStatusProductionOrder', 'ProductionOrderController@setStatusProductionOrder');
});



/*
Route::apiResource("providers","ProvidersController");
Route::apiResource("typerecords","TypeRecordController");  
*/


