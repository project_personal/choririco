<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formula_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('master_id')->unsigned();
            $table->integer('raw_material_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->integer('ordering');
            $table->float('quantity');
            $table->foreign('unit_id')->references('id')->on('unit_measurements'); 
            $table->foreign('raw_material_id')->references('id')->on('raw_materials');
            $table->foreign('master_id')->references('id')->on('master_formulas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formula_details');
    }
}
