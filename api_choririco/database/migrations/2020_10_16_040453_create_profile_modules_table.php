<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profiles_id')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->foreign('profiles_id')->references('id')->on('profiles');
            $table->foreign('module_id')->references('id')->on('modules');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_modules');
    }
}
