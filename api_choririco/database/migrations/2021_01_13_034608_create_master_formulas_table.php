<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterFormulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_formulas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('observation');
            $table->integer('unit_id')->unsigned();
            $table->float('quantity');
            $table->foreign('unit_id')->references('id')->on('unit_measurements'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_formulas');
    }
}
