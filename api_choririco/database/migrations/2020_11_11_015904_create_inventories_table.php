<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unit_id')->unsigned();
            $table->integer('raw_material_id')->unsigned();
            $table->integer('grocery_id')->unsigned();
            $table->float('amount');
            $table->string('movement_type');
            $table->string('price');
            $table->string('observation');
            $table->foreign('unit_id')->references('id')->on('unit_measurements'); 
            $table->foreign('raw_material_id')->references('id')->on('raw_materials');
            $table->foreign('grocery_id')->references('id')->on('groceries');
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
