import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterFormulaComponent } from './master-formula.component';

describe('MasterFormulaComponent', () => {
  let component: MasterFormulaComponent;
  let fixture: ComponentFixture<MasterFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
