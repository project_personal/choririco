import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, FormBuilder , Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

import {switchMap, debounceTime} from 'rxjs/operators';
import {Observable} from 'rxjs'
import {Material, IMaterialResponse} from '../class/rawmaterial';

@Component({
  selector: 'app-master-formula',
  templateUrl: './master-formula.component.html',
  styleUrls: ['./master-formula.component.css']
})

export class MasterFormulaComponent implements OnInit {

  public _formGroup:FormGroup;
  public submitted:boolean;
  public unidades:any;
  public unidades2:any;
  public btnDisabled:boolean;
  public detailFormula:any[];
  public products:any;

  filteredUsers: Observable<IMaterialResponse>;

  displayedColumns: string[] = ['id', 'description','product_id', 'observation','unit_id','quantity' ,'created_at', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private fb: FormBuilder,private Jarwis: JarwisService) { 
    this.createFormGroup();
    this.submitted = false;
    this.detailFormula = [];
  }

  createFormGroup(){
    this._formGroup = this.fb.group({
      material: null,
      id: null,
      description: ['', Validators.required],
      observation: ['', Validators.required],
      product_id: ['', Validators.required],
      unit_id: ['', Validators.required],
      quantity: ['', Validators.required],
      unit_id2: null,
      quantity2: null
    })
	} 

  getListFormulas() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('formulamaestra').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  onDelete(formula){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('formulamaestra',formula).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  addMaterial(){
    console.log(this.unidades)
    let material = this._formGroup.get('material').value;
    let quantity = this._formGroup.get('quantity2').value;
    let unit = this._formGroup.get('unit_id2').value; 
    let dataUnit = this.unidades.find(x => x.id == unit);
    if(material != null){
      if(material.id !== undefined && quantity !==null && unit !== null ){
        this.detailFormula.push({material: material, quantity: quantity, unit_id:unit, unit: dataUnit.unit });
        this._formGroup.controls['material'].setValue(""); 
        this._formGroup.controls['quantity2'].setValue(""); 
      }
    }
  }

  removeMaterial(detail){
    if(confirm('Desea Eliminar la materia prima')){
      let index = this.detailFormula.findIndex(x => x.material.id == detail.material.id)
      this.detailFormula.splice(index, 1); 
    }
  }

  onEdit(formula){
    this._formGroup.controls['id'].setValue(formula.id);
		this._formGroup.controls['description'].setValue(formula.description);
    this._formGroup.controls['observation'].setValue(formula.observation);
    this._formGroup.controls['unit_id'].setValue(formula.unit_id);
    this._formGroup.controls['quantity'].setValue(formula.quantity);
    this._formGroup.controls['product_id'].setValue(formula.product_id);
    console.log(formula.formula_detail)
    let dataDetail = [];
    formula.formula_detail.forEach(function(e){
      dataDetail.push({material: e.raw_material, quantity: e.quantity, unit_id: e.unit_id, unit: e.unit_measurement.unit });
    })
    this.detailFormula = dataDetail;
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.dataSource = new MatTableDataSource(data.formula_maestra);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.unidades= data.unidades;
    this.unidades2= data.unidades2;
    this.products= data.products;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)
  } 

  ngOnInit() {
    this.getListFormulas();
    this.filteredUsers = this._formGroup.get('material').valueChanges
      .pipe(
        debounceTime(300),
        switchMap(value => this.Jarwis.search({description: value}, 1, this.detailFormula))
      );
  }

  displayFn(material: Material) {
    if (material) { return material.description; }
  }

  onSubmit() {
    $('.loaderBox').show();
    this.submitted = true; 
    let dataFormula = this._formGroup.value;
    dataFormula['detail'] = this.detailFormula;
    console.log(this._formGroup.get('id').value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('formulamaestra',dataFormula).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('formulamaestra',dataFormula).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

  onResetForm(){
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
    this.detailFormula = [];
  }
   
  get f() { return this._formGroup.controls; }

}
