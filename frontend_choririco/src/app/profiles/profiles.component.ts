import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnInit {

  public modules:any;
  public _formGroup:FormGroup;
  public submitted:boolean;
  displayedColumns: string[] = ['id', 'description', 'observation', 'created_at', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Jarwis: JarwisService) { 
    this._formGroup = this.createFormGroup();
    this.submitted = false;
  }

  createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			description: new FormControl('',[Validators.required]),
      observation: new FormControl('',[Validators.required]),
      modules : new FormControl('',[Validators.required]),
		}); 
	} 

  getListProfiles() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('profiles').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  onDelete(profile){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('profiles',profile).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  getListModules() {
    this.Jarwis.getRecords('modules').subscribe(
      data => {
        this.modules = data;
      } 
    );
  }

  onEdit(profile){
    var arrayModules = new Array(); 
    profile.modules.forEach(function(item){
      arrayModules.push(item.id);
    });
    this._formGroup.controls['id'].setValue(profile.id);
		this._formGroup.controls['description'].setValue(profile.description);
    this._formGroup.controls['observation'].setValue(profile.observation);
    this._formGroup.controls['modules'].setValue(arrayModules);
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)

  }

  ngOnInit() {
    this.getListProfiles();
    this.getListModules();
  }
  onSubmit() {
    console.log(this._formGroup.value);
    $('.loaderBox').show();
    this.submitted = true; 
    console.log(this._formGroup.value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('profiles',this._formGroup.value).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('profiles',this._formGroup.value).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onResetForm(){
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
  }
  
  get f() { return this._formGroup.controls; }
}
