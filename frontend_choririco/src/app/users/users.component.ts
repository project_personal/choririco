import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ErrorClass } from '../class/ErrorClass';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit  {

  public profiles:Object;
  public _formGroup:FormGroup;
  public submitted:boolean;
  public error:ErrorClass;

  displayedColumns: string[] = ['id', 'name', 'id_profile', 'email', 'created_at', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Jarwis: JarwisService) { 
    this._formGroup = this.createFormGroup();
    this.submitted = false;
    this.error = new ErrorClass(); 
  }

  createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			id_profile: new FormControl('',[Validators.required]),
      name: new FormControl('',[Validators.required,Validators.minLength(8)]),
      email: new FormControl('',[Validators.required,Validators.email]),
      password: new FormControl('',[Validators.required,Validators.minLength(8)]),
      password_confirmation: new FormControl('',[Validators.required]),
		}); 
	} 

  getListUsers() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('users').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  getListProfiles() {
    this.Jarwis.getRecords('profiles').subscribe(
      data => {
        this.profiles = data;
      } 
    );
  }

  onDelete(user){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('users',user).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  onEdit(user){
    this._formGroup.controls['id'].setValue(user.id);
		this._formGroup.controls['name'].setValue(user.name);
    this._formGroup.controls['email'].setValue(user.email);
    this._formGroup.controls['id_profile'].setValue(user.id_profile);
    this._formGroup.controls['password'].setValue('');
    this._formGroup.controls['password_confirmation'].setValue('');
    $('#txtEmail').attr('disabled','true');
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  handleError(data){
    $('.loaderBox').hide();
    this.error = data.error.errors;
    console.log(data)

  }

  ngOnInit() {
    this.getListProfiles();
    this.getListUsers();
  }
  onSubmit() {
    $('.loaderBox').show();
    this.submitted = true; 
    console.log(this._formGroup.value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('users',this._formGroup.value).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.signup(this._formGroup.value).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onResetForm(){
    this._formGroup.reset();
    this.error =null;
    this._formGroup.controls['id'].setValue(""); 
    $('#txtEmail').removeAttr('disabled');
  }  
  
  get f() { return this._formGroup.controls; }

}
