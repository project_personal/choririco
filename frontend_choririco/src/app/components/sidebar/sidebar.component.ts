import { Component, OnInit } from '@angular/core';
import { JarwisService } from '../../services/jarwis.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
    { path: '/profiles', title: 'Perfiles',  icon:'people', class: '' },
    { path: '/users', title: 'Usuarios',  icon:'recent_actors', class: '' },
    { path: '/modules', title: 'Módulos',  icon:'view_module', class: '' },
    //{ path: '/providers', title: 'Proveedores',  icon:'person', class: '' },
    //{ path: '/login', title: 'Login',  icon:'person', class: '' },
    //{ path: '/signup', title: 'Signup',  icon:'person', class: '' },
    //{ path: '/profile', title: 'Profile',  icon:'person', class: '' },
    //{ path: '/request-reset', title: 'request-reset',  icon:'person', class: '' },
    //{ path: '/response-reset', title: 'response-reset',  icon:'person', class: '' },
    //{ path: '/table-list', title: 'Table List',  icon:'content_paste', class: '' },
    //{ path: '/typography', title: 'Typography',  icon:'library_books', class: '' },
    { path: '/icons', title: 'Icons',  icon:'bubble_chart', class: '' },
    //{ path: '/maps', title: 'Maps',  icon:'location_on', class: '' },
    //{ path: '/notifications', title: 'Notifications',  icon:'notifications', class: '' },
    //{ path: '/upgrade', title: 'Upgrade to PRO',  icon:'unarchive', class: 'active-pro' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  public profile:string;

  constructor(private Jarwis: JarwisService) { 
    this.profile = localStorage.getItem('profile');
  }

  ngOnInit() {
    console.log( ROUTES.filter(menuItem => menuItem));
    //this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.getListModules();
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };

  getListModules() {
    $('.loaderBox').show();
    this.Jarwis.getRecord('profiles',this.profile).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data){
    $('.loaderBox').hide();
    data[0].modules.forEach(function(item){
      item.title = item.description;
      item.class = '';
    });
    this.menuItems = data[0].modules.filter(menuItem => menuItem);
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)

  }
}
