import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  public departments:any;
  public cities:any;
  public zonas:any;
  public citiesFinal:any;
  public documents:any;
  public _formGroup:FormGroup;
  public submitted:boolean;

  displayedColumns: string[] = ['id', 'document_type_id','name', 'email', 'phone', 'mobile', 'address', 'city_id',"zone_id", 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Jarwis: JarwisService) { 
    this._formGroup = this.createFormGroup();
    this.submitted = false;
  }

  createFormGroup(){ 
		return new FormGroup({
			id: new FormControl(''),
      document_type_id: new FormControl('',[Validators.required]),
			document_number: new FormControl('',[Validators.required]),
      zone_id: new FormControl('',[Validators.required]),
      name: new FormControl('',[Validators.required,Validators.minLength(6)]),
      email: new FormControl('',[Validators.required,Validators.email]),
      phone: new FormControl('',[Validators.minLength(7)]),
      mobile: new FormControl('',[Validators.required,Validators.minLength(10)]),
      address: new FormControl('',[Validators.required]),
      department_id: new FormControl('',[Validators.required]),
      city_id: new FormControl('',[Validators.required]),
		}); 
  } 
  
  onEdit(customer){
    this.citiesFinal = this.cities.filter(element => element.department_id == customer.department_id);
    this._formGroup.controls['id'].setValue(customer.id);
		this._formGroup.controls['document_type_id'].setValue(customer.document_type_id);
    this._formGroup.controls['document_number'].setValue(customer.document_number);
    this._formGroup.controls['name'].setValue(customer.name);
    this._formGroup.controls['zone_id'].setValue(customer.zone_id);
    this._formGroup.controls['email'].setValue(customer.email);
    this._formGroup.controls['phone'].setValue(customer.phone);
    this._formGroup.controls['mobile'].setValue(customer.mobile);
    this._formGroup.controls['address'].setValue(customer.address);
    this._formGroup.controls['department_id'].setValue(customer.department_id);
    this._formGroup.controls['city_id'].setValue(customer.city_id);
  }

  getListCustomers() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('customers').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  getCities(event){
    this.citiesFinal = this.cities.filter(element => element.department_id == event.value);
  }

  onDelete(customer){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('customers',customer).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.departments = data.departments;
    this.cities = data.cities;
    this.zonas = data.zonas;
    this.documents = data.documents;
    this.dataSource = new MatTableDataSource(data.customers);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)

  }

  ngOnInit() {
    this.getListCustomers();
  }
  onSubmit() {
    $('.loaderBox').show();
    this.submitted = true; 
    console.log(this._formGroup.value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('customers',this._formGroup.value).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('customers',this._formGroup.value).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

  onResetForm(){
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
  }
  
  get f() { return this._formGroup.controls; }

}
