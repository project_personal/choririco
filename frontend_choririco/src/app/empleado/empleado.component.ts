import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  public zonas:any;
  public documents:any;
  public zonaArray:any;
  public _formGroup:FormGroup;
  public submitted:boolean;

  displayedColumns: string[] = ['id', 'document_type_id', 'name', 'email', 'mobile', 'address', 'zones','created_at',"action"];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Jarwis: JarwisService) { 
    this._formGroup = this.createFormGroup();
    this.submitted = false;
  }

  createFormGroup(){  
		return new FormGroup({
			id: new FormControl(''),
      document_type_id: new FormControl('',[Validators.required]),
			document_number: new FormControl('',[Validators.required]),
      name: new FormControl('',[Validators.required,Validators.minLength(6)]),
      email: new FormControl('',[Validators.required,Validators.email]),
      mobile: new FormControl('',[Validators.required,Validators.minLength(10)]),
      address: new FormControl('',[Validators.required]),
      zonas : new FormControl('',[Validators.required])
		}); 
  } 
  
  onEdit(employee){
    var arrayZones = new Array(); 
    employee.zones.forEach(function(item){
      arrayZones.push(item.id);
    });
    this._formGroup.controls['id'].setValue(employee.id);
		this._formGroup.controls['document_type_id'].setValue(employee.document_type_id);
    this._formGroup.controls['document_number'].setValue(employee.document_number);
    this._formGroup.controls['name'].setValue(employee.name);
    this._formGroup.controls['email'].setValue(employee.email);
    this._formGroup.controls['mobile'].setValue(employee.mobile);
    this._formGroup.controls['address'].setValue(employee.address);
    this._formGroup.controls['zonas'].setValue(arrayZones);
  }

  getListEmployees() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('employees').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  onDelete(employee){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('employees',employee).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.zonas = data.zonas;
    this.documents = data.documents;
    this.dataSource = new MatTableDataSource(data.employees);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)

  }

  ngOnInit() {
    this.getListEmployees();
  }

  onSubmit() {
    $('.loaderBox').show();
    this.submitted = true; 
    console.log(this._formGroup.value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('employees',this._formGroup.value).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('employees',this._formGroup.value).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

  onResetForm(){
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
  }
  
  get f() { return this._formGroup.controls; }


}
