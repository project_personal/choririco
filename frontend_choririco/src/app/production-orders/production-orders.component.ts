import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import { FormGroup, FormBuilder , Validators} from '@angular/forms';


@Component({
  selector: 'app-production-orders',
  templateUrl: './production-orders.component.html',
  styleUrls: ['./production-orders.component.css']
})
export class ProductionOrdersComponent implements OnInit {

  public _formGroup:FormGroup;
  public submitted:boolean;
  public btnDisabled:boolean;
  public btnDisabled2:boolean;
  public detailFormula:any[];
  public formulas:any;
  public statusOrder:string;
  public statusTab:string;
  public tabsStatus:any[];

  constructor(private fb: FormBuilder,private Jarwis: JarwisService) { 
    this.createFormGroup();
    this.submitted = false;
    this.btnDisabled = true;
    this.btnDisabled2 = true;
    this.statusTab ='Pendiente';
    this.tabsStatus = ['Pendiente','En Proceso','En Empaque','Almacenamiento','Finalizado','Cancelado'];
  }

  onShowOrdersTab(a){
    this.statusTab = this.tabsStatus[a.index];
  }

  createFormGroup(){
    this._formGroup = this.fb.group({
      id: null,
      amount: ['', Validators.required],
      master_formula_id: ['', Validators.required],
    })
	} 

  getListOrders() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('productionOrder').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  calcularDetalleOrder(){
    if(!this._formGroup.invalid){
      let cant = this._formGroup.value['amount']; 
      $('.loaderBox').show();
      this.Jarwis.postRecord('detailFormula',this._formGroup.value).subscribe(
        data => {
          let arrayData =[];
          arrayData.push(data);
          $('.loaderBox').hide();
          let valBoolean = false; 
          arrayData[0].forEach(function(e){
            let cant2 =e['cantRequerida'];
            if(e['descUnitFormula'] !== e['descUnitMateria']){
              if(e['gramosMateria'] < e['gramosFormula']){
                cant2 = (e['gramosFormula']/e['gramosMateria'])*e['cantRequerida'];
              }else{
                cant2 = (e['cantRequerida']*e['gramosFormula'])/e['gramosMateria'];
              }
            }
            e['totalRequerida'] = cant2*cant;
            if(e['totalDisponible'] ==null){
              e['totalDisponible'] =0;
            }
            e['total'] = e['totalDisponible']-e['totalRequerida'];
            if(e['total'] < 0){
              valBoolean=true;
            }

          });
          this.btnDisabled=valBoolean;
          this.detailFormula = arrayData[0];
        }
      );
    }else{
      $('#btnGuardar').click();
    }
  }

  handleResponse(data){
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.formulas= data.formula_maestra;
    
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)
  } 

  ngOnInit() {
    this.getListOrders();
  }


  onSubmit() {
    $('.loaderBox').show();
    this.statusTab = this.statusTab+'|actualizar';
    this.submitted = true; 
    let dataFormula = this._formGroup.value;
    dataFormula['detail'] = this.detailFormula;
    if(!this._formGroup.invalid){
      this.Jarwis.postRecord('productionOrder',dataFormula).subscribe(
        data => this.handleResponse(data),
        error => this.handleError(error)
      );
    }else{
      $('.loaderBox').hide();
    }
  }

  onResetForm(){
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
    this.detailFormula = [];
    this.btnDisabled = false;
    this.btnDisabled2 = false;
    this._formGroup.controls['amount'].enable();
    this._formGroup.controls['master_formula_id'].enable();
  }

  recieveInformationOrder($event){
    this._formGroup.controls['amount'].disable();
    this._formGroup.controls['master_formula_id'].disable();
    this._formGroup.controls['amount'].setValue($event.amount);
    this._formGroup.controls['master_formula_id'].setValue($event.master_formula_id);
    let dataFinal = [];
    $event.order_detail.forEach(function(e){
      dataFinal.push({IdMateria: e.raw_material_id, NameMateria: e.raw_material.description, totalRequerida: e.required_quantity, totalDisponible: e.quantity_available, descUnitMateria: e.unit_measurement.unit, total: e.quantity_available-e.required_quantity });
    });
    this.btnDisabled=true;
    this.btnDisabled2=true;
    this.detailFormula = dataFinal; 
  }
   
  get f() { return this._formGroup.controls; }


}
