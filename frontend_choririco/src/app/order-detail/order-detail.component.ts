import { Component,OnChanges , Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort} from '@angular/material/sort';
import { MatTableDataSource} from '@angular/material/table';
import { JarwisService } from '../services/jarwis.service';


@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnChanges {
  
  private _estado:string;
  private dataOrder:{ status: string };
  private nextStatus:any[];

  @Output() detalleOrdenEmitter = new EventEmitter<any[]>();

  @Input()
  set estado(estado:string) {
    let arr = estado.split('|');
    this._estado = (arr[0]) || '<no personname set>';
  }

  displayedColumns: string[] = ['id', 'master_formula_id','product','amount','quantity','unit_id','total','created_at','updated_at', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
 
  constructor(private Jarwis: JarwisService) { 
    this.nextStatus = [{'Pendiente':'En Proceso','En Proceso':'En Empaque','En Empaque':'Almacenamiento','Almacenamiento':'Finalizado'}];
  }   

  ngOnChanges() {
    this.getListOrders();
  }   

  getListOrders() {
    $('.loaderBox').show();
    this.dataOrder = {'status':this._estado};
    this.Jarwis.postRecord('getProductionOrder',this.dataOrder).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data){
    $('.loaderBox').hide();
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    //$('#btnCerrar').click();
    
  }

  handleError(data){
    //$('.loaderBox').hide();
    console.log(data)
  } 

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onCancelete(item){
    if(confirm('¿Realmente desea cancelar la orden?')){
      $('.loaderBox').show();
      Object.assign(item,{newStatus: 'Cancelado'})
      this.Jarwis.postRecord('setStatusProductionOrder',item).subscribe(
        data => this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  onChangeStatus(item){ 
    if(confirm('¿Desea actualizar el estado de la Orden?')){
      $('.loaderBox').show();
      Object.assign(item,{newStatus: this.nextStatus[0][this._estado]})
      this.Jarwis.postRecord('setStatusProductionOrder',item).subscribe(
        data => this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  onView(item){
    console.log(item)
    this.detalleOrdenEmitter.emit(item);
  }

}
