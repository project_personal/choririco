import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {

  public modules:any;
  public _formGroup:FormGroup;
  public submitted:boolean;

  displayedColumns: string[] = ['id', 'description', 'observation', 'icon', 'path', 'created_at', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Jarwis: JarwisService) { 
    this._formGroup = this.createFormGroup();
    this.submitted = false;
  }

  createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			description: new FormControl('',[Validators.required]),
      observation: new FormControl('',[Validators.required]),
      icon: new FormControl('',[Validators.required]),
      path: new FormControl('',[Validators.required]),
		}); 
	} 

  getListModules() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('modules').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  onDelete(module){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('modules',module).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  onEdit(module){
    this._formGroup.controls['id'].setValue(module.id);
		this._formGroup.controls['description'].setValue(module.description);
    this._formGroup.controls['observation'].setValue(module.observation);
    this._formGroup.controls['icon'].setValue(module.icon);
    this._formGroup.controls['path'].setValue(module.path);
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.modules = data;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)

  }

  ngOnInit() {
    this.getListModules();
  }
  onSubmit() {
    $('.loaderBox').show();
    this.submitted = true; 
    console.log(this._formGroup.value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('modules',this._formGroup.value).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('modules',this._formGroup.value).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onResetForm(){
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
  }
  
  get f() { return this._formGroup.controls; }
}
