import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-modules',
  templateUrl: './providers.component.html'
})


export class ProvidersComponent implements OnInit {

  public departments:any;
  public cities:any;
  public citiesFinal:any;
  public _formGroup:FormGroup;
  public submitted:boolean;

  displayedColumns: string[] = ['id', 'nit', 'compamy_name', 'email', 'phone', 'movil', 'address', 'city_id', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Jarwis: JarwisService) { 
    this._formGroup = this.createFormGroup();
    this.submitted = false;
  }

  createFormGroup(){ 
		return new FormGroup({
			id: new FormControl(''),
			nit: new FormControl('',[Validators.required]),
      compamy_name: new FormControl('',[Validators.required,Validators.minLength(6)]),
      email: new FormControl('',[Validators.required,Validators.email]),
      phone: new FormControl('',[Validators.minLength(7)]),
      movil: new FormControl('',[Validators.required,Validators.minLength(10)]),
      address: new FormControl('',[Validators.required]),
      department_id: new FormControl('',[Validators.required]),
      city_id: new FormControl('',[Validators.required]),
		}); 
  } 
  
  onEdit(provider){
    this.citiesFinal = this.cities.filter(element => element.department_id == provider.department_id);
    this._formGroup.controls['id'].setValue(provider.id);
		this._formGroup.controls['nit'].setValue(provider.nit);
    this._formGroup.controls['compamy_name'].setValue(provider.compamy_name);
    this._formGroup.controls['email'].setValue(provider.email);
    this._formGroup.controls['phone'].setValue(provider.phone);
    this._formGroup.controls['movil'].setValue(provider.movil);
    this._formGroup.controls['address'].setValue(provider.address);
    this._formGroup.controls['department_id'].setValue(provider.department_id);
    this._formGroup.controls['city_id'].setValue(provider.city_id);
  }

  getListModules() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('providers').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  getCities(event){
    this.citiesFinal = this.cities.filter(element => element.department_id == event.value);
  }

  onDelete(provider){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('providers',provider).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  recibirDatos(event){
		console.log(event.nombre);
	}

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.departments = data.departments;
    this.cities = data.cities;
    this.dataSource = new MatTableDataSource(data.providers);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)

  }

  ngOnInit() {
    this.getListModules();
  }
  onSubmit() {
    $('.loaderBox').show();
    this.submitted = true; 
    console.log(this._formGroup.value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('providers',this._formGroup.value).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('providers',this._formGroup.value).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

  onResetForm(){
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
  }
  
  get f() { return this._formGroup.controls; }
}
