import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { JarwisService } from '../services/jarwis.service';


@Component({
  selector: 'app-stock-materiaprima',
  templateUrl: './stock-materiaprima.component.html',
  styleUrls: ['./stock-materiaprima.component.css']
})
export class StockMateriaprimaComponent implements AfterViewInit {

  displayedColumns: string[] = ['id', 'description', 'unit', 'bodega', 'totalEntrada', 'totalSalida', 'price'];
  dataSource: MatTableDataSource<Element[]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Jarwis: JarwisService) { }


  getListMaterial() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('stock').subscribe(
      data => this.handleResponse(data)
    );
  }

  handleResponse(data){
    console.log(data);
    $('.loaderBox').hide();
    this.dataSource = new MatTableDataSource(data.stockInventario);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngAfterViewInit() {
    this.getListMaterial();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

