import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockMateriaprimaComponent } from './stock-materiaprima.component';

describe('StockMateriaprimaComponent', () => {
  let component: StockMateriaprimaComponent;
  let fixture: ComponentFixture<StockMateriaprimaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockMateriaprimaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockMateriaprimaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
