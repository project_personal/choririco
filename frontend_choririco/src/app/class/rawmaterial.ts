export class Material {
  constructor(public id: number, public description: string) {}
}

export interface IMaterialResponse {
  total: number;
  results: Material[];
  rawMaterial : Material[];
}