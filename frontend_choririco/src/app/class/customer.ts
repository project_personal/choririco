export class Customer {
    constructor(public id: string, public name:string, public document_type:any ,public document_number:string , public address:string, public zone_id:string) {}
  }
  
  export interface ICustomerResponse {
    total: number;
    results: Customer[];
    customers : Customer[];
  }