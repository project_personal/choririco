import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {map , tap} from 'rxjs/operators';
import {Material, IMaterialResponse} from '../class/rawmaterial';
import { Customer, ICustomerResponse } from '../class/customer';

@Injectable()    
export class JarwisService { 
  private baseUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient) { 
  }  

  signup(data) {
    return this.http.post(`${this.baseUrl}/signup`, data)
  }

  login(data) {
    return this.http.post(`${this.baseUrl}/login`, data)
  }

  sendPasswordResetLink(data) {
    return this.http.post(`${this.baseUrl}/sendPasswordResetLink`, data)
  }
  
  changePassword(data) {
    return this.http.post(`${this.baseUrl}/resetPassword`, data)
  }

  getRecords(endpoint){
    return this.http.get(`${this.baseUrl}/${endpoint}`);
  }

  getRecord(endpoint, id){
    return this.http.get(`${this.baseUrl}/${endpoint}/${id}`);
  }

  postRecord(endpoint, data) {
    return this.http.post(`${this.baseUrl}/${endpoint}`, data)
  }

  putRecord(endpoint, data) {
    return this.http.put(`${this.baseUrl}/${endpoint}/${data.id}`, data)
  }

  delteRecord(endpoint, data) {
    //return this.http.delete(`${this.baseUrl}/${endpoint}/${data.id}`, data)
    return this.http.request('delete', `${this.baseUrl}/${endpoint}/${data.id}`, {body: data})
  }

  search(filter: {description: string} = {description: ''}, page = 1, detailFormula ): Observable<IMaterialResponse> {
    return this.http.get<IMaterialResponse>(`${this.baseUrl}/rawMaterial`)
    .pipe(
      tap((response: IMaterialResponse) => {
        response.results = response.rawMaterial
          .map(material => new Material(material.id, material.description))
          .filter(function(material){
            var validar =false;
            if(detailFormula.length > 0){
              var data = detailFormula.filter(x => x.material.id == material.id);
              if(data.length > 0){
                validar = true;
              }
            }
            
            if(material.description.toLowerCase().includes(filter.description) && !validar){
              return true;
            }
          })
        return response;
      })
      );
  }

  searchCustomer(filter: {name: string} = {name: ''}, page = 1 ): Observable<ICustomerResponse> {
    return this.http.get<ICustomerResponse>(`${this.baseUrl}/customers`)
    .pipe(
      tap((response: ICustomerResponse) => { 
        response.results = response.customers
          .map(customer => new Customer(customer.id, customer.name , customer.document_type.symbol ,customer.document_number, customer.address, customer.zone_id))
          .filter(function(Customer){
            if(Customer.name.toLowerCase().includes(filter.name)){
              return true;
            }
          })
        return response;
      })
      );
  }

}