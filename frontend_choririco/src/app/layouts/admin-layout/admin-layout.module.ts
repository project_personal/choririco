import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { MatTableModule } from '@angular/material/table';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { ProvidersComponent } from '../../providers/providers.component';
import { ProfilesComponent } from '../../profiles/profiles.component';
import { UsersComponent } from '../../users/users.component';
import { ModulesComponent } from '../../modules/modules.component';
import { MatButtonModule} from '@angular/material/button';
import { MatInputModule} from '@angular/material/input';
import { MatRippleModule} from '@angular/material/core';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatTooltipModule} from '@angular/material/tooltip'; 
import { MatSelectModule} from '@angular/material/select';
import { DatatableComponent } from '../../datatable/datatable.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { SignupComponent } from '../../authentication/signup/signup.component'; 
import { ProfileComponent } from '../../authentication/profile/profile.component'; 
import { RequestResetComponent } from '../../authentication/password/request-reset/request-reset.component'; 
import { ResponseResetComponent } from '../../authentication/password/response-reset/response-reset.component';
import { RawMaterialComponent } from '../../raw-material/raw-material.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ProductComponent } from '../../product/product.component';
import { GroceryComponent } from '../../grocery/grocery.component';
import { InventoriesComponent } from '../../inventories/inventories.component';
import { FormatNumberDirective } from '../../format-number.directive';
import { MyCurrencyPipe } from '../../my-currency.pipe';
import { StockMateriaprimaComponent } from '../../stock-materiaprima/stock-materiaprima.component';
import { MasterFormulaComponent } from '../../master-formula/master-formula.component';
import { ProductionOrdersComponent } from '../../production-orders/production-orders.component';
import { OrderDetailComponent } from '../../order-detail/order-detail.component';
import {MatTabsModule} from '@angular/material/tabs';
import { CustomerComponent } from '../../customer/customer.component';
import { ZonaComponent } from '../../zona/zona.component';
import { EmpleadoComponent } from '../../empleado/empleado.component';
import { OrderRequestComponent } from '../../order-request/order-request.component';
   
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    MatTabsModule,
    MatPaginatorModule,
    MatTableModule,
    NgSelectModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatTooltipModule,
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    StockMateriaprimaComponent,
    TableListComponent,
    TypographyComponent,
    ProvidersComponent,
    ZonaComponent,
    EmpleadoComponent,
    OrderDetailComponent,
    OrderRequestComponent,
    GroceryComponent,
    CustomerComponent,
    ProductComponent,
    ProfilesComponent,
    ProductionOrdersComponent,
    InventoriesComponent,
    RawMaterialComponent,
    DatatableComponent,
    ModulesComponent,
    UsersComponent,
    MasterFormulaComponent,
    ProfileComponent,
    RequestResetComponent,
    ResponseResetComponent,
    SignupComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    FormatNumberDirective,
    MyCurrencyPipe
  ]
})

export class AdminLayoutModule {}
