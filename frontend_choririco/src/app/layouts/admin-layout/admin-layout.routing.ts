import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { ProvidersComponent } from '../../providers/providers.component';
import { ProfilesComponent } from '../../profiles/profiles.component';
import { ModulesComponent } from '../../modules/modules.component';
import { LoginComponent } from '../../authentication/login/login.component'; 
import { SignupComponent } from '../../authentication/signup/signup.component'; 
import { ProfileComponent } from '../../authentication/profile/profile.component'; 
import { UsersComponent } from '../../users/users.component';
import { RequestResetComponent } from '../../authentication/password/request-reset/request-reset.component'; 
import { ResponseResetComponent } from '../../authentication/password/response-reset/response-reset.component'; 
import { BeforeLoginService } from '../../services/before-login.service';
import { AfterLoginService } from '../../services/after-login.service';
import { RawMaterialComponent } from '../../raw-material/raw-material.component';
import { ProductComponent } from '../../product/product.component';
import { GroceryComponent } from '../../grocery/grocery.component';
import { InventoriesComponent } from '../../inventories/inventories.component';
import { StockMateriaprimaComponent } from '../../stock-materiaprima/stock-materiaprima.component';
import { MasterFormulaComponent } from '../../master-formula/master-formula.component';
import { ProductionOrdersComponent } from '../../production-orders/production-orders.component';
import { CustomerComponent } from '../../customer/customer.component';
import { ZonaComponent } from '../../zona/zona.component';
import { EmpleadoComponent } from '../../empleado/empleado.component';
import { OrderRequestComponent } from '../../order-request/order-request.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent , canActivate: [AfterLoginService] },
    { path: 'user-profile',   component: UserProfileComponent , canActivate: [AfterLoginService] },
    { path: 'table-list',     component: TableListComponent , canActivate: [AfterLoginService] },
    { path: 'typography',     component: TypographyComponent, canActivate: [AfterLoginService]  },
    { path: 'icons',          component: IconsComponent, canActivate: [AfterLoginService]  },
    { path: 'providers',      component: ProvidersComponent, canActivate: [AfterLoginService]  },
    { path: 'groceries',      component: GroceryComponent, canActivate: [AfterLoginService]  },
    { path: 'stockMp',        component: StockMateriaprimaComponent, canActivate: [AfterLoginService]  },
    { path: 'request',        component: OrderRequestComponent, canActivate: [AfterLoginService]  },
    { path: 'production-orders', component: ProductionOrdersComponent, canActivate: [AfterLoginService]  },
    { path: 'customers',      component: CustomerComponent, canActivate: [AfterLoginService]  },
    { path: 'profiles',       component: ProfilesComponent, canActivate: [AfterLoginService]  },
    { path: 'zonas',          component: ZonaComponent, canActivate: [AfterLoginService]  },
    { path: 'empleados',      component: EmpleadoComponent, canActivate: [AfterLoginService]  },
    { path: 'master-formula', component: MasterFormulaComponent, canActivate: [AfterLoginService]  },
    { path: 'rawmaterial',    component: RawMaterialComponent, canActivate: [AfterLoginService] },
    { path: 'product',        component: ProductComponent, canActivate: [AfterLoginService]  },
    { path: 'inventories',    component: InventoriesComponent, canActivate: [AfterLoginService] },
    { path: 'modules',        component: ModulesComponent, canActivate: [AfterLoginService]  },
    { path: 'login',          component: LoginComponent , canActivate: [BeforeLoginService] }, 
    { path: 'signup',         component: SignupComponent , canActivate: [AfterLoginService] }, 
    { path: 'profile',        component: ProfileComponent , canActivate: [AfterLoginService] },
    { path: 'users',          component: UsersComponent , canActivate: [AfterLoginService] }, 
    { path: 'request-reset',  component: RequestResetComponent , canActivate: [BeforeLoginService] },  
    { path: 'response-reset', component: ResponseResetComponent , canActivate: [BeforeLoginService] }, 
    { path: 'maps',           component: MapsComponent, canActivate: [AfterLoginService] },
    { path: 'notifications',  component: NotificationsComponent , canActivate: [AfterLoginService] },
    { path: 'upgrade',        component: UpgradeComponent, canActivate: [AfterLoginService] }, 
    { path: 'upgrade',        component: UpgradeComponent, canActivate: [AfterLoginService] }, 
];
