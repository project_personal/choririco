import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import { FormGroup, FormBuilder , Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {jsPDF} from 'jspdf';
import html2canvas from 'html2canvas';
import {switchMap, debounceTime} from 'rxjs/operators';
import {Observable} from 'rxjs'
import {Customer, ICustomerResponse} from '../class/customer';

export class DataPdf{
  constructor(
    public customerName: string, 
    public document:string,
    public address:string,
    public mobile:string,
    public zone:string,
    public mensajero:string,
    public delivery_date:string,
    public observation:string,
    public request_detail:any[]
  ){}
}

@Component({
  selector: 'app-order-request',
  templateUrl: './order-request.component.html',
  styleUrls: ['./order-request.component.css']
})

export class OrderRequestComponent implements OnInit {

  public _formGroup:FormGroup;
  public submitted:boolean;
  public zonas:any;
  public empleados:any;
  public empleados2:any[];
  public products:any[];
  public products2:any[];
  public btnDisabled:boolean;
  public detailFormula:any[];
  public btnView:boolean;
  public nameCustomer:string;

  filteredCustomer: Observable<ICustomerResponse>;

  displayedColumns: string[] = ['id', 'description','product_id', 'observation','unit_id','quantity' ,'created_at', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public dataPdf:DataPdf;

  constructor(private fb: FormBuilder,private Jarwis: JarwisService) { 
    this.createFormGroup();
    this.submitted = false;
    this.detailFormula = [];
    this.btnView = false;
    this.nameCustomer ='';
    this.dataPdf = new DataPdf('','','','','','','','',[]);
  }

  createFormGroup(){
    this._formGroup = this.fb.group({
      customer_id: ['', Validators.required],
      id: null,
      zone_id: ['', Validators.required],
      address: ['', Validators.required],
      employee_id: ['', Validators.required],
      delivery_date: ['', Validators.required],
      observation: ['', Validators.required],
      product_id: null,
      quantity: null
    })
	} 

  getList() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('request').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  onDelete(order){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('request',order).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  addProduct(){
    let product = this._formGroup.get('product_id').value;
    let quantity = this._formGroup.get('quantity').value;
    if(product != null && quantity !==null && quantity > 0){
      let productObject = this.products.find(p => p.id == product);
      if(quantity <= productObject.quantity_available){
        this.detailFormula.push({codigo: productObject.id, cantRequerida: quantity, cantDisponible:productObject.quantity_available, product: productObject.description});
        this._formGroup.controls['product_id'].setValue(""); 
        this._formGroup.controls['quantity'].setValue(""); 
        this.selectProducts();
      }else{
        alert('No existe inventario en bodega');
      }
    }
  }

  removeMaterial(detail){
    if(confirm('Desea Eliminar el producto terminado')){
      let index = this.detailFormula.findIndex(x => x.codigo == detail.codigo)
      this.detailFormula.splice(index, 1); 
      this.selectProducts();
    }
  }

  onView(order){
    this.btnView = true;
    console.log(order);
    this.selectEmployee(order.zone_id);
    this.nameCustomer = order.customer.name
    this._formGroup.controls['id'].setValue(order.id);
    this._formGroup.controls['zone_id'].setValue(order.zone_id);
    this._formGroup.controls['address'].setValue(order.address);
    this._formGroup.controls['employee_id'].setValue(order.employee_id);
    this._formGroup.controls['observation'].setValue(order.observation);
    this._formGroup.controls['delivery_date'].setValue(order.delivery_date);
    console.log(order.formula_detail)
    let dataDetail = [];
    order.request_detail.forEach(function(e){
      dataDetail.push({codigo: e.product_id, cantRequerida: e.quantity, cantDisponible: e.product.quantity_available, product: e.product.description});
    })
    this.detailFormula = dataDetail;
    this._formGroup.controls['address'].disable();
    this._formGroup.controls['zone_id'].disable();
    this._formGroup.controls['employee_id'].disable();
    this._formGroup.controls['observation'].disable();
    this._formGroup.controls['delivery_date'].disable();
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.dataSource = new MatTableDataSource(data.requests);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.empleados= data.employee;
    this.products= data.products;
    this.zonas= data.zone;
    this.selectProducts();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)
  } 

  ngOnInit() {
    this.getList();
    this.filteredCustomer = this._formGroup.get('customer_id').valueChanges
      .pipe(
        debounceTime(300),
        switchMap(value => this.Jarwis.searchCustomer({name: value}, 1))
      );
  }

  displayFn(customer: Customer) {
    if (customer) { return customer.name; }
  }

  onSubmit() {
    $('.loaderBox').show();
    this.submitted = true; 
    let dataFormula = this._formGroup.value;
    dataFormula['detail'] = this.detailFormula;
    console.log(dataFormula);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('request',dataFormula).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('request',dataFormula).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

  onResetForm(){
    this.btnView = false;
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
    this.detailFormula = [];
    this._formGroup.controls['address'].enable();
    this._formGroup.controls['zone_id'].enable();
    this._formGroup.controls['employee_id'].enable();
    this._formGroup.controls['observation'].enable();
    this._formGroup.controls['delivery_date'].enable();
  }
  
  onChangeCustomer(customer){
    var data = customer.option.value;
    this._formGroup.controls['address'].setValue(data.address); 
    this._formGroup.controls['zone_id'].setValue(data.zone_id); 
    this.selectEmployee(data.zone_id);
  }

  selectEmployee(zone_id){
    this._formGroup.controls['employee_id'].setValue(''); 
    var arrayEmployee = []
    this.empleados.forEach(function(e){
      if(e.zones.length > 0){
        e.zones.forEach(function(a){
          if(a.id == zone_id){
            arrayEmployee.push(e);
          }
        })
      }
    })
    this.empleados2 = arrayEmployee;
  }

  getEmployees(event){
    this.selectEmployee(event.value);
  }

  selectProducts(){
      let detailFormulaClone = this.detailFormula;
      let productsClone = []
      this.products.forEach(function(e){
        let val = detailFormulaClone.find(x=> x.codigo == e.id);
        if(val == undefined){
          productsClone.push(e);
        }
      });
      this.products2=productsClone;
  }

  get f() { return this._formGroup.controls; }
  
  @ViewChild('htmlData') htmlData:ElementRef;
  
  

  public openPDF(row):void {
    console.log(row);
    $('.loaderBox').show();
    $('.containerPdf').show();
    this.dataPdf = new DataPdf(row.customer.name,row.customer.document_number,row.address,row.customer.mobile,row.zone.name,row.employee.name, row.delivery_date,row.observation,row.request_detail);
    setTimeout(function(){
      let DATA = document.getElementById('htmlData');
      html2canvas(DATA).then(canvas => {
          let fileWidth = 220;
          let fileHeight = canvas.height * fileWidth / canvas.width;
          const FILEURI = canvas.toDataURL('image/png')
          let PDF = new jsPDF('p', 'mm', 'a4');
          let position = 10;
          PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight)
          PDF.save('documentoDespacho.pdf');
          $('.loaderBox').hide();
          $('.containerPdf').hide();
      });     
    },1000);
    
  }
 
}
