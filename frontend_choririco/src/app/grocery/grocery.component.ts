import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-grocery',
  templateUrl: './grocery.component.html',
  styleUrls: ['./grocery.component.css']
})
export class GroceryComponent implements OnInit {

  public _formGroup:FormGroup;
  public submitted:boolean;

  displayedColumns: string[] = ['id', 'description', 'observation', 'created_at', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Jarwis: JarwisService) { 
    this._formGroup = this.createFormGroup();
    this.submitted = false;
  }

  createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			description: new FormControl('',[Validators.required]),
      observation: new FormControl('',[Validators.required]),
		}); 
	} 

  getListGroceries() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('groceries').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  onDelete(grocery){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('groceries',grocery).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  onEdit(grocery){
    this._formGroup.controls['id'].setValue(grocery.id);
		this._formGroup.controls['description'].setValue(grocery.description);
    this._formGroup.controls['observation'].setValue(grocery.observation);
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)
  }

  ngOnInit() {
    this.getListGroceries();
  }

  onSubmit() {
    console.log(this._formGroup.value);
    $('.loaderBox').show();
    this.submitted = true; 
    console.log(this._formGroup.value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('groceries',this._formGroup.value).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('groceries',this._formGroup.value).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

  onResetForm(){
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
  }
  
  get f() { return this._formGroup.controls; }
}
