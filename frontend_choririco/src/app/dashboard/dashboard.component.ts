import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { JarwisService } from '../services/jarwis.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public dataInforme:any[];
  public dataZones:any[];
  public dataOrders:any[];

  constructor(private Jarwis: JarwisService) { 
    this.dataInforme = [{'informe':''},{'informe':''},{'informe':''},{'informe':''}];
    this.dataZones = [];
    this.dataOrders = [];
  }

  startAnimationForLineChart(chart){
      let seq: any, delays: any, durations: any;
      seq = 0;
      delays = 80;
      durations = 500;

      chart.on('draw', function(data) {
        if(data.type === 'line' || data.type === 'area') {
          data.element.animate({
            d: {
              begin: 600,
              dur: 700,
              from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
              to: data.path.clone().stringify(),
              easing: Chartist.Svg.Easing.easeOutQuint
            }
          });
        } else if(data.type === 'point') {
              seq++;
              data.element.animate({
                opacity: {
                  begin: seq * delays,
                  dur: durations,
                  from: 0,
                  to: 1,
                  easing: 'ease'
                }
              });
          }
      });

      seq = 0;
  };
  startAnimationForBarChart(chart){
      let seq2: any, delays2: any, durations2: any;

      seq2 = 0;
      delays2 = 80;
      durations2 = 500;
      chart.on('draw', function(data) {
        if(data.type === 'bar'){
            seq2++;
            data.element.animate({
              opacity: {
                begin: seq2 * delays2,
                dur: durations2,
                from: 0,
                to: 1,
                easing: 'ease'
              }
            });
        }
      });

      seq2 = 0;
  };
  ngOnInit() {
      this.getInformacion();
  }
  
  getInformacion() {
    this.Jarwis.getRecords('dashboard').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data){
    console.log(data)
    this.dataInforme = data.informacion;
    this.dataZones = data.zones;
    this.dataOrders = data.orders;

    /* ----------==========     Completed Tasks Chart initialization    ==========---------- */
    
    let labels = [];
    let series2 = [];
    this.dataOrders.forEach(function(e){
      labels.push(e.proceso)
      series2.push(e.cantidad)
    });
    console.log(labels)
    const dataCompletedTasksChart: any = {
      labels: labels,
      series: [
        series2
      ]
  };

 const optionsCompletedTasksChart: any = {
      lineSmooth: Chartist.Interpolation.cardinal({
          tension: 0
      }),
      low: 0,
      high: 10, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
      chartPadding: { top: 0, right: 0, bottom: 0, left: 0}
  }

  var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);

  // start animation for the Completed Tasks Chart - Line Chart
  this.startAnimationForLineChart(completedTasksChart);

  /* ----------==========     Emails Subscription Chart initialization    ==========---------- */
  console.log(this.dataZones);
  labels = [];
  series2 = [];
  this.dataZones.forEach(function(e){
    labels.push(e.name)
    series2.push(e.cantidad)
  });
  console.log(series2)
  var datawebsiteViewsChart = {
    labels: labels,
    series: [
      series2
    ]
  };
  var optionswebsiteViewsChart = {
      axisX: {
          showGrid: false
      },
      low: 0,
      high: 10,
      chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
  };
  var responsiveOptions: any[] = [
    ['screen and (max-width: 640px)', {
      seriesBarDistance: 5,
      axisX: {
        labelInterpolationFnc: function (value) {
          return value[0];
        }
      }
    }]
  ];
  var websiteViewsChart = new Chartist.Bar('#websiteViewsChart', datawebsiteViewsChart, optionswebsiteViewsChart, responsiveOptions);

  //start animation for the Emails Subscription Chart
  this.startAnimationForBarChart(websiteViewsChart);
  }

  handleError(data){
    console.log(data)
  }

}
