import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-inventories',
  templateUrl: './inventories.component.html',
  styleUrls: ['./inventories.component.css']
})
export class InventoriesComponent implements OnInit {

  public materias:any;
  public bodegas:any;
  public types:any;
  public unidades:any;
  public _formGroup:FormGroup;
  public submitted:boolean;

  displayedColumns: string[] = ['id', 'raw_material', 'grocery', 'movement_type', 'created_at', 'unit_measurement', 'amount', 'document', 'price', 'document_value', 'observation', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private Jarwis: JarwisService) { 
    this._formGroup = this.createFormGroup();
    this.submitted = false;
  }

  createFormGroup(){ 
		return new FormGroup({
			id: new FormControl(''),
      raw_material_id: new FormControl('',[Validators.required]),
      grocery_id: new FormControl('',[Validators.required]),
      amount: new FormControl('',[Validators.required]),
      movement_type_id: new FormControl('',[Validators.required]),
      document_value: new FormControl(''),
      document: new FormControl(''),
      observation: new FormControl(''),
		}); 
  } 

  getListMovimientos() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('inventories').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  onDelete(inventory){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('inventories',inventory).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    if(data.code !== '406'){
      $('#btnCerrar').click();
      this.materias = data.materias;
      console.log(data.materias)
      this.bodegas = data.bodegas;
      this.types = data.types;
      this.dataSource = new MatTableDataSource(data.movimientos);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }else{
      $('#alert_message').html(data.message);
      $('#alert_message').show(200);
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    console.log(this.dataSource.filter);
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)

  }

  ngOnInit() {
    this.getListMovimientos();
  }
  onSubmit() {
    $('.loaderBox').show();
    this.submitted = true; 
    console.log(this._formGroup.value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('inventories',this._formGroup.value).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('inventories',this._formGroup.value).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

  onView(inventory){
    this._formGroup.controls['raw_material_id'].disable();
    this._formGroup.controls['grocery_id'].disable();
    this._formGroup.controls['amount'].disable();
    this._formGroup.controls['movement_type_id'].disable();
    this._formGroup.controls['observation'].disable();
    this._formGroup.controls['document_value'].disable();
    this._formGroup.controls['document'].disable();

    this._formGroup.controls['raw_material_id'].setValue(inventory.raw_material_id);
    this._formGroup.controls['grocery_id'].setValue(inventory.grocery_id);
    this._formGroup.controls['amount'].setValue(inventory.amount);
    this._formGroup.controls['movement_type_id'].setValue(inventory.movement_type_id);
    this._formGroup.controls['observation'].setValue(inventory.observation);
    this._formGroup.controls['document_value'].setValue(inventory.document_value);
    this._formGroup.controls['document'].setValue(inventory.document);
  }

  onResetForm(){
    this._formGroup.controls['raw_material_id'].enable();
    this._formGroup.controls['grocery_id'].enable();
    this._formGroup.controls['amount'].enable();
    this._formGroup.controls['movement_type_id'].enable();
    this._formGroup.controls['observation'].enable();
    this._formGroup.controls['document_value'].enable();
    this._formGroup.controls['document'].enable();

    $('#alert_message').hide();
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
  }
  
  get f() { return this._formGroup.controls; }
}
