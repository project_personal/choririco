import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  public typeProducts:any;
  public _formGroup:FormGroup;
  public submitted:boolean;
  displayedColumns: string[] = ['id', 'description', 'observation', 'type_product', 'created_at','quantity_available', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  ngOnInit() {
    this.getListProducts();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onResetForm(){
		this._formGroup.reset();
    this._formGroup.controls['id'].setValue(""); 
  }

  constructor(private Jarwis: JarwisService) { 
    this._formGroup = this.createFormGroup();
    this.submitted = false;
  }

  createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			description: new FormControl('',[Validators.required]),
      observation: new FormControl('',[Validators.required]),
      typeProduct_id: new FormControl('',[Validators.required]),
		}); 
	} 

  getListProducts() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('products').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  onDelete(materia){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('products',materia).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  onEdit(materia){
    this._formGroup.controls['id'].setValue(materia.id);
		this._formGroup.controls['description'].setValue(materia.description);
    this._formGroup.controls['observation'].setValue(materia.observation);
    this._formGroup.controls['typeProduct_id'].setValue(materia.typeProduct_id);
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.typeProducts = data.typeProducts;
    this.dataSource = new MatTableDataSource(data.products);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)

  }

  onSubmit() {
    console.log(this._formGroup.value);
    $('.loaderBox').show();
    this.submitted = true; 
    console.log(this._formGroup.value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('products',this._formGroup.value).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('products',this._formGroup.value).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }
  
  get f() { return this._formGroup.controls; }

}
