import { Component, OnInit, ViewChild } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-raw-material',
  templateUrl: './raw-material.component.html',
  styleUrls: ['./raw-material.component.css']
})
export class RawMaterialComponent implements OnInit {

  public materias:any;
  public providers:any;
  public _formGroup:FormGroup;
  public submitted:boolean;
  public unidades:any;
  displayedColumns: string[] = ['id', 'description', 'provider_id', 'unit_id', 'amount', 'price', 'observation', 'created_at', 'action'];
  dataSource: MatTableDataSource<Element[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.getListMaterial();
  }
  onResetForm(){
		this._formGroup.reset();
    this._formGroup.controls['id'].setValue(""); 
  }

  constructor(private Jarwis: JarwisService) { 
    this._formGroup = this.createFormGroup();
    this.submitted = false;
  }

  createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			description: new FormControl('',[Validators.required]),
      observation: new FormControl('',[Validators.required]),
      provider_id: new FormControl('',[Validators.required]),
      unit_id: new FormControl('',[Validators.required]),
      price: new FormControl('',[Validators.required]),
      amount: new FormControl('',[Validators.required]),
		}); 
	} 

  getListMaterial() {
    $('.loaderBox').show();
    this.Jarwis.getRecords('rawMaterial').subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  onDelete(materia){
    if(confirm("¿Desea Eliminar este registro?")){
      $('.loaderBox').show();
      this.Jarwis.delteRecord('rawMaterial',materia).subscribe(
        data =>this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }


  onEdit(materia){
    this._formGroup.controls['id'].setValue(materia.id);
		this._formGroup.controls['description'].setValue(materia.description);
    this._formGroup.controls['observation'].setValue(materia.observation);
    this._formGroup.controls['provider_id'].setValue(materia.provider_id);
    this._formGroup.controls['unit_id'].setValue(materia.unit_id);
    this._formGroup.controls['price'].setValue(materia.price);
    this._formGroup.controls['amount'].setValue(materia.amount);
  }

  handleResponse(data){
    console.log(data)
    $('.loaderBox').hide();
    $('#btnCerrar').click();
    this.materias = data.rawMaterial;
    this.providers = data.providers;
    this.unidades = data.unidades;
    this.dataSource = new MatTableDataSource(data.rawMaterial);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  handleError(data){
    $('.loaderBox').hide();
    console.log(data)

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onSubmit() {
    console.log(this._formGroup.value);
    $('.loaderBox').show();
    this.submitted = true; 
    console.log(this._formGroup.value);
    if(!this._formGroup.invalid){
      if(this._formGroup.get('id').value !== ''){
        this.Jarwis.putRecord('rawMaterial',this._formGroup.value).subscribe(
          data =>this.handleResponse(data),
          error => this.handleError(error)
        );
      }else{
        this.Jarwis.postRecord('rawMaterial',this._formGroup.value).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      }
    }else{
      $('.loaderBox').hide();
    }
  }

 
  
  get f() { return this._formGroup.controls; }

}
